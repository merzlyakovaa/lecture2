package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranching_Test {

    /**
     * Проверка возвращаемого значения {@link MyBranching#maxInt(int, int)}.
     * Сценарий, при котором {@link Utils#utilFunc1(String)} и {@link Utils#utilFunc2()} возвращают
     * <code>false</code>.
     */
    @Test
    public void maxIntWith2False_Test1() {
        final int i1 = 2;
        final int i2 = 4;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(i2, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка возвращаемого значения {@link MyBranching#maxInt(int, int)}.
     * Сценарий, при котором {@link Utils#utilFunc1(String)} и {@link Utils#utilFunc2()} возвращают
     * <code>false</code>.
     */
    @Test
    public void maxIntWith2True_Test1() {
        final int i1 = 2;
        final int i2 = 4;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка возвращаемого значения {@link MyBranching#maxInt(int, int)}.
     * Сценарий, при котором {@link Utils#utilFunc1(String)} и {@link Utils#utilFunc2()} возвращают
     * <code>false</code>.
     */
    @Test
    public void maxIntWithTrueFalse_Test1() {
        final int i1 = 2;
        final int i2 = 4;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(i2, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка возвращаемого значения {@link MyBranching#maxInt(int, int)}.
     * Сценарий, при котором {@link Utils#utilFunc1(String)} и {@link Utils#utilFunc2()} возвращают
     * <code>false</code>.
     */
    @Test
    public void maxIntWithFalseTrue_Test1() {
        final int i1 = 2;
        final int i2 = 4;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(4)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка возвращаемого значения {@link MyBranching#maxInt(int, int)}.
     * Сценарий, при котором {@link Utils#utilFunc1(String)} и {@link Utils#utilFunc2()} возвращают
     * <code>false</code>.
     */
    @Test
    public void maxIntWithTrueFalseDifferentInputParams_Test1() {
        final int i1 = 1;
        final int i2 = 4;

        Utils utilsMock = Mockito.mock(Utils.class);

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(i2, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void ifElseExampleTrue_Test() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        Assertions.assertTrue(myBranching.ifElseExample());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void ifElseExampleFalse_Test() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);
        Assertions.assertFalse(myBranching.ifElseExample());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleWith1_Test() {
        int i = 1;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleWith2_Test() {
        int i = 2;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleWith0andTrue_Test() {
        int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleWith0andFalse_Test() {
        int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }
}
